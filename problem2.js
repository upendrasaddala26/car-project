module.exports = function problem2(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        return []
    } else {
        return ("Last car is a " + inventory[inventory.length - 1].car_make + " " + inventory[inventory.length - 1].car_model)

    }
}