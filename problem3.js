module.exports = function (inventory) {

    if (Array.isArray(inventory)&&inventory.length>0) {
        let model = inventory.sort((a, b) => a.car_model.toLowerCase() > b.car_model.toLowerCase() ? 1 : -1);
        return model;
    }
    else {

        return []
    }
}
