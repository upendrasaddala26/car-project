function problem1(inventory=[], id="") {
    if (   !Array.isArray(inventory) || (typeof id !== "number") || (inventory.length ===0) ) {
        return [];
    }
    else {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].id == id){
                return inventory[i];
            }
        }
        return []
    }

}

module.exports = problem1